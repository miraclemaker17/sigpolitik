import * as firebase from 'firebase';
import firestore from 'firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyAcXfUcvmPDwVGFS5oHX7v9Oj_Z3zlWXfg",
    authDomain: "sigpol-17.firebaseapp.com",
    databaseURL: "https://sigpol-17.firebaseio.com",
    projectId: "sigpol-17",
    storageBucket: "sigpol-17.appspot.com",
    messagingSenderId: "50002567882",
    appId: "1:50002567882:web:303f595edf66aebfd44a33"
};

firebase.initializeApp(firebaseConfig);

firebase.firestore();

export default firebase;